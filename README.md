Am I allowed to redistribute this pack?

Yes. Please check the license.

Does Soartex Grove have the right to create a texture pack with Soar49's textures and Invictus?

Yes, Soartex is in the public domain. Shoeboxam gave me permission to update.

*Why do so many textures look similar or appear to be from the Fanver continuation?

Aside from direct contributors to Invictus, all graphics added by Invictus were created by Shoeboxam. When I wrote the Soartex Fanver license, I included the line: "The original artist of any texture may reuse their own texture elsewhere." This permits me to reuse my own repertoire of content I created for Fanver in Invictus instead- I created many of the textures in Fanver. So because I receive permission from Shoeboxam i can use it.

Do I need to use Optifine?

The pack can be played without, but the pack looks better with Optifine due to the water color, better skies and lightmaps.

Where I can check updates?

https://gitlab.com/GroveGraphics/soartex-grove
